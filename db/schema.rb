# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190713071455) do

  create_table "cita", force: :cascade do |t|
    t.integer "cotizaccionId"
    t.date "fecha"
    t.time "hora"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cotizacions", force: :cascade do |t|
    t.integer "clientId"
    t.string "estado"
    t.integer "departamentoId"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departamentos", force: :cascade do |t|
    t.integer "proyectoId"
    t.string "nombre"
    t.integer "area"
    t.integer "piso"
    t.string "estado"
    t.string "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "proyecto_id"
    t.index ["proyecto_id"], name: "index_departamentos_on_proyecto_id"
  end

  create_table "estados", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fotos", force: :cascade do |t|
    t.integer "item"
    t.string "nombre"
    t.string "imagen"
    t.integer "departamento_id"
    t.integer "proyecto_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departamento_id"], name: "index_fotos_on_departamento_id"
    t.index ["proyecto_id"], name: "index_fotos_on_proyecto_id"
  end

  create_table "perfils", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proformas", force: :cascade do |t|
    t.integer "cotizaccionId"
    t.datetime "fechaRegistro"
    t.float "costo"
    t.float "descuento"
    t.float "precioFinal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proyectos", force: :cascade do |t|
    t.string "nombre"
    t.string "ubicacion"
    t.float "preciobase"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipos", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string "dni"
    t.string "nombres"
    t.string "apellidos"
    t.string "correo"
    t.string "telefono"
    t.string "perfil"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "perfil_id"
    t.index ["perfil_id"], name: "index_usuarios_on_perfil_id"
  end

end
