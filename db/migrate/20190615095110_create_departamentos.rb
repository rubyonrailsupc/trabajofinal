class CreateDepartamentos < ActiveRecord::Migration[5.1]
  def change
    create_table :departamentos, id: false do |t|
      t.integer :id, primary_key: true
      t.integer :proyectoId
      t.string :nombre
      t.integer :area
      t.integer :piso
      t.string :estado
      t.string :tipo

      t.timestamps
    end
  end
end
