class CreateFotos < ActiveRecord::Migration[5.1]
  def change
    create_table :fotos do |t|
      t.integer :item
      t.string :nombre
      t.string :imagen
      t.references :departamento, foreign_key: true
      t.references :proyecto, foreign_key: true

      t.timestamps
    end
  end
end
