class AddReferenceToUsuario < ActiveRecord::Migration[5.1]
  def change
     add_reference :usuarios, :perfil, foreign_key: true
  end
end
