class AddProyectoToDepartamento < ActiveRecord::Migration[5.1]
  def change
    add_reference :departamentos, :proyecto, foreign_key: true
  end
end
