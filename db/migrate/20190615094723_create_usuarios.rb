class CreateUsuarios < ActiveRecord::Migration[5.1]
  def change
    create_table :usuarios, id: false do |t|
      t.integer :id, primary_key: true
      t.string :dni
      t.string :nombres
      t.string :apellidos
      t.string :correo
      t.string :telefono
      t.string :perfil

      t.timestamps
    end
  end
end
