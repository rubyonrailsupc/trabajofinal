class CreateProformas < ActiveRecord::Migration[5.1]
  def change
    create_table :proformas, id: false do |t|
      t.primary_key :id, primary_key: true
      t.integer :cotizaccionId
      t.datetime :fechaRegistro
      t.float :costo
      t.float :descuento
      t.float :precioFinal

      t.timestamps
    end
  end
end
