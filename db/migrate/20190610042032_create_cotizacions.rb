class CreateCotizacions < ActiveRecord::Migration[5.1]
  def change
    create_table :cotizacions, id: false do |t|
      t.integer :id, primary_key: true
      t.integer :clientId
      t.string :estado
      t.integer :departamentoId

      t.timestamps
    end
  end
end
