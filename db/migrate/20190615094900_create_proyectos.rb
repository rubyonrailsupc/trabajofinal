class CreateProyectos < ActiveRecord::Migration[5.1]
  def change
    create_table :proyectos, id: false do |t|
   	  t.integer :id, primary_key: true
      t.string :nombre
      t.string :ubicacion
      t.float :preciobase

      t.timestamps
    end
  end
end
