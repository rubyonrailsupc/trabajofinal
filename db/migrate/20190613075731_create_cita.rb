class CreateCita < ActiveRecord::Migration[5.1]
  def change
    create_table :cita, id: false do |t|
      t.primary_key :id, primary_key: true
      t.integer :cotizaccionId
      t.date :fecha
      t.time :hora

      t.timestamps
    end
  end
end
