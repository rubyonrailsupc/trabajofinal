class DepartamentosController < ApplicationController
  before_action :set_departamento, only: [:show, :edit, :update, :destroy]
  layout 'internal_administrador'
  # GET /departamentos
  # GET /departamentos.json
  def index
    
    pNombre = "%#{params[:nombre]}%"  
    pEstado = "%#{params[:estado]}%"
    

    if params[:proyecto_id] == "0"
       pProyectoId = "%%" 
      puts "paso"
    else
       pProyectoId = "%#{params[:proyecto_id]}%" 
    end

    @departamentos = Departamento.paginate(page: params[:page], per_page:5).where("id !=0 and nombre like ? and proyecto_id like ? and estado like ?", pNombre.to_s, pProyectoId.to_s, pEstado.to_s)

    @proyectos = Proyecto.all 
  end

  # GET /departamentos/1
  # GET /departamentos/1.json
  def show
    @fotos = @departamento.fotos.all

    respond_to do |f|
      f.js
    end
  end

  # GET /departamentos/new
  def new
    @departamento = Departamento.new
    @foto = @departamento.fotos.build

    respond_to do |f|
      f.js
    end
  end

  # GET /departamentos/1/edit
  def edit
    respond_to do |f|
      f.js
    end
  end

  # POST /departamentos
  # POST /departamentos.json
  def create
    
    @departamento = Departamento.new(departamento_params)

    respond_to do |format|
      if @departamento.save

        params[:fotos]['imagen'].each do |a|
          @foto = @departamento.fotos.create!( :imagen => a, :proyecto_id => 0, :departamento_id =>@departamento.id )
        end  

        format.html { redirect_to departamentos_url, notice: 'Departamento was successfully created.' }
        format.json { render :show, status: :created, location: @departamento }
       
      else
        format.html { render :new }
        format.json { render json: @departamento.errors, status: :unprocessable_entity }
        format.js   { render layout: false, content_type: 'text/javascript' }
      end
    end

  end

  # PATCH/PUT /departamentos/1
  # PATCH/PUT /departamentos/1.json
  def update
    respond_to do |format|
      if @departamento.update(departamento_params)
        format.html { redirect_to departamentos_url, notice: 'Departamento was successfully updated.' }
        format.json { render :show, status: :ok, location: @departamento }
      else
        format.html { render :edit }
        format.json { render json: @departamento.errors, status: :unprocessable_entity }
        format.js   { render layout: false, content_type: 'text/javascript' }
      end
    end
  end

  # DELETE /departamentos/1
  # DELETE /departamentos/1.json
  def destroy
    @departamento.destroy
    respond_to do |format|
      format.html { redirect_to departamentos_url, notice: 'Departamento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_departamento
      @departamento = Departamento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def departamento_params
      params.require(:departamento).permit(:id, :nombre, :area, :piso, :estado, :tipo, :proyecto_id, fotos_attributes: [:id, :departamento_id, :imagen])
      #params.require(:departamento).permit(:id, :proyectoId, :nombre, :area, :piso, :estado, :tipo, :proyecto_id)
    end
end
