class UsuariosController < ApplicationController
  before_action :set_usuario, only: [:show, :edit, :update, :destroy]
  layout 'internal_administrador'
  # GET /usuarios
  # GET /usuarios.json
  def index

    pNombre = "%#{params[:nombres]}%"  
    pPerfil = "%#{params[:perfil_id]}%"
    

    if params[:perfil_id] == "0"
       pPerfil = "%%" 
      puts "paso"
    else
       pPerfil = "%#{params[:perfil_id]}%"
    end



    @usuarios = Usuario.paginate(page: params[:page], per_page:5).where("nombres like ? and perfil_id like ?", pNombre.to_s, pPerfil.to_s)


    @perfiles = Perfil.all 
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
        respond_to do |f|
      f.js
    end
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new

    respond_to do |f|
      f.js
    end
  end

  # GET /usuarios/1/edit
  def edit
     respond_to do |f|
      f.js
    end
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to usuarios_url, notice: 'Usuario was successfully created.' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
        format.js   { render layout: false, content_type: 'text/javascript' }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to usuarios_url, notice: 'Usuario was successfully updated.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
        format.js   { render layout: false, content_type: 'text/javascript' }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:id, :dni, :nombres, :apellidos, :correo, :telefono, :perfil, :perfil_id)
    end
end
