class ProyectosController < ApplicationController
  before_action :set_proyecto, only: [:show, :edit, :update, :destroy]
  layout 'internal_administrador'
  # GET /proyectos
  # GET /proyectos.json
  def index
    pNombre = "%#{params[:nombre]}%" 
    @proyectos = Proyecto.paginate(page: params[:page], per_page:5).where("id !=0 and nombre like ?", pNombre.to_s)
    
  end

  # GET /proyectos/1
  # GET /proyectos/1.json
  def show
    @fotos = @proyecto.fotos.all
    respond_to do |f|
      f.js
    end
  end

  # GET /proyectos/new
  def new
    @proyecto = Proyecto.new
    @foto = @proyecto.fotos.build

    respond_to do |f|
      f.js
    end
  end

  # GET /proyectos/1/edit
  def edit
    respond_to do |f|
      f.js
    end
  end

  # POST /proyectos
  # POST /proyectos.json
  def create
    @proyecto = Proyecto.new(proyecto_params)

    respond_to do |format|
      if @proyecto.save

        params[:fotos]['imagen'].each do |a|
          @foto = @proyecto.fotos.create!( :imagen => a, :proyecto_id => @proyecto.id, :departamento_id => 0 )
        end   

        format.html { redirect_to proyectos_url, notice: 'Proyecto was successfully created.' }
        format.json { render :show, status: :created, location: @proyecto }
      else
        format.html { render :new }
        format.json { render json: @proyecto.errors, status: :unprocessable_entity }
        format.js   { render layout: false, content_type: 'text/javascript' }

        #format.html { flash.now[:alert] = "Not done" render "new" }
        #format.json { render json: {:errors => @proyecto.errors}, status: :unprocessable_entity }
        #format.js { render :js=>'alert("not done");' } 
      end
    end
  end

  # PATCH/PUT /proyectos/1
  # PATCH/PUT /proyectos/1.json
  def update
    respond_to do |format|
      if @proyecto.update(proyecto_params)
        format.html { redirect_to proyectos_url, notice: 'Proyecto was successfully updated.' }
        format.json { render :show, status: :ok, location: @proyecto }
      else
        format.html { render :edit }
        format.json { render json: @proyecto.errors, status: :unprocessable_entity }
        format.js   { render layout: false, content_type: 'text/javascript' }
      end
    end
  end

  # DELETE /proyectos/1
  # DELETE /proyectos/1.json
  def destroy
    @foto.destroy
    @proyecto.destroy
    respond_to do |format|
      format.html { redirect_to proyectos_url, notice: 'Proyecto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_proyecto
      @proyecto = Proyecto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def proyecto_params
      #params.require(:proyecto).permit(:id, :nombre, :ubicacion, :preciobase)
      params.require(:proyecto).permit(:id, :nombre, :ubicacion, :preciobase, fotos_attributes: [:id, :proyecto_id, :imagen]) 
    end
end
