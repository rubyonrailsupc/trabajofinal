class Usuario < ApplicationRecord

    belongs_to :perfil

	validates :dni , presence: true
	validates :nombres, length: { maximum: 150 } , presence: {
		message: "es obligatorio"
	}
	validates :apellidos, length: { maximum: 150 } , presence: {
		message: "es obligatorio"
	}
	validates :correo, length: { maximum: 150 } , presence: {
		message: "es obligatorio"
	}

	validates :telefono, numericality: true , presence: true
	validates :perfil, length: { maximum: 150 }, presence: true

end