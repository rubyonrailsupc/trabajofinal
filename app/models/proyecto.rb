class Proyecto < ApplicationRecord

	#Implementacion de imagenes
	has_many :fotos
	accepts_nested_attributes_for :fotos
	
	has_many :departamento

	validates :nombre, length: { maximum: 150 }, presence: true
	validates :ubicacion, presence: true
	validates :preciobase, numericality: true, presence: true 

end