class Foto < ApplicationRecord

  mount_uploader :imagen, PictureUploader
  belongs_to :departamento
  belongs_to :proyecto
end
