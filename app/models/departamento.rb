class Departamento < ApplicationRecord
	
	#Implementacion de imagenes
	has_many :fotos
	accepts_nested_attributes_for :fotos
	
	belongs_to :proyecto
	
	validates :proyecto_id , presence: true
	validates :nombre, length: { maximum: 150 } , presence: {
		message: "es obligatorio"
	}
	validates :area, numericality: true 
	validates :piso, numericality: true 
	validates :estado, length: { maximum: 150 }
	validates :tipo, length: { maximum: 150 } 
	
	
end