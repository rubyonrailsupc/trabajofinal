// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .

function ValidarProyecto(){

	if ($("#proyecto_nombre").val() == '') {
		alert('Ingrese Nombre');
		$("#proyecto_nombre").focus();
		return false;
	}
	else if($("#proyecto_ubicacion").val() == '') {
		alert('Ingrese Ubicación');
		$("#proyecto_ubicacion").focus();
		return false;
	}
	else if($("#proyecto_preciobase").val() == '' || $("#proyecto_preciobase").val() == '0') {
		alert('Ingrese Precio');
		$("#proyecto_preciobase").focus();
		return false;
	}
	else{
		
		//var resultado = confirm("¿Esta Seguro de Guardar?");
		if (confirm("¿Esta Seguro de Guardar?")) {
			alert('Registrado correctamente');
		} 
		else {
			return false;
		}
	}

}


function ValidarUsuario(){

	if ($("#usuario_dni").val() == '') {
		alert('Ingrese DNI');
		$("#usuario_dni").focus();
		return false;
	}
	else if($("#usuario_nombres").val() == '') {
		alert('Ingrese Nombres');
		$("#usuario_nombres").focus();
		return false;
	}
	else if($("#usuario_apellidos").val() == '') {
		alert('Ingrese Apellidos');
		$("#usuario_apellidos").focus();
		return false;
	}
	else if($("#usuario_correo").val() == '') {
		alert('Ingrese Correo');
		$("#usuario_correo").focus();
		return false;
	}	
	else if($("#usuario_telefono").val() == '' || $("#usuario_telefono").val() == '0') {
		alert('Ingrese Telefono');
		$("#usuario_telefono").focus();
		return false;
	}
	else if($("#usuario_perfil_id").val() == ''  || $("#usuario_perfil_id").val() == '0') {
		alert('Ingrese Perfil');
		$("#usuario_perfil_id").focus();
		return false;
	}
	else{
		
		//var resultado = confirm("¿Esta Seguro de Guardar?");
		if (confirm("¿Esta Seguro de Guardar?")) {
			alert('Registrado correctamente');
		} 
		else {
			return false;
		}
	}
}



function ValidarDepartamento(){

	if ($("#departamento_proyecto_id").val() == ''  || $("#departamento_proyecto_id").val() == '0') {
		alert('Seleccione Proyecto');
		$("#departamento_proyecto_id").focus();
		return false;
	}
	else if($("#departamento_nombre").val() == '') {
		alert('Ingrese Nombre');
		$("#departamento_nombre").focus();
		return false;
	}
	else if($("#departamento_area").val() == '' || $("#departamento_area").val() == '0') {
		alert('Ingrese Area');
		$("#departamento_area").focus();
		return false;
	}
	else if($("#departamento_piso").val() == '' || $("#departamento_piso").val() == '0') {
		alert('Ingrese Piso');
		$("#departamento_piso").focus();
		return false;
	}
	else if($("#departamento_tipo").val() == '' || $("#departamento_tipo").val() == '0') {
		alert('Seleccione Tipo');
		$("#departamento_tipo").focus();
		return false;
	}
	else if($("#departamento_estado").val() == '' || $("#departamento_estado").val() == '0') {
		alert('Seleccione Estado');
		$("#departamento_estado").focus();
		return false;
	}
	else{
		
		//var resultado = confirm("¿Esta Seguro de Guardar?");
		if (confirm("¿Esta Seguro de Guardar?")) {
			alert('Registrado correctamente');
		} 
		else {
			return false;
		}
	}

}