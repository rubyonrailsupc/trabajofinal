Rails.application.routes.draw do

  resources :perfils
  resources :estados
  resources :tipos
  resources :fotos
  get 'solicitud/index'

  resources :departamentos
  resources :proyectos
  resources :usuarios
  root 'home#index'

  resources :proformas
  resources :cita 
  resources :cotizacions

  get 'home/proyecto'
  get 'cotizacions/index'
  #get 'proformas/index'
  #get 'index', controller: :proformas, action: :index, alias: 'index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
